# Base Image
FROM debian:bullseye-slim

# Install 
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

# Copy web service files
COPY Kalender-H.py /home/kalender-hijriyah/Kalender-H.py
COPY requirements.txt /home/kalender-hijriyah/requirements.txt

# Install Python dependencies
RUN pip3 install --no-cache-dir -r /home/kalender-hijriyah/requirements.txt

# Set working directory
WORKDIR /home/kalender-hijriyah

# Expose port 
EXPOSE 25014

# Start web service 
CMD ["streamlit", "run", "--server.port=25014", "Kalender-H.py"]