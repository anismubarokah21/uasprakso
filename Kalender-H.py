import streamlit as st
import pandas as pd

# Data kalender hijriyah
data = {
    'Bulan': ['Muharram', 'Muharram', 'Muharram', 'Rabi\' al-Awwal', 'Jumadil Awal', 'Jumadil Akhir',
              'Rajab', 'Ramadhan', 'Ramadhan', 'Syawal', 'Dzulhijjah', 'Dzulhijjah'],
    'Tanggal': [1, 9, 10, 12, 27, 15, 27, 1, 27, 1, 9, 10],
    'Peringatan': ['Tahun Baru Hijriyah', 'Tasua', 'Ashura', 'Maulid Nabi Muhammad', 'Isra\' Mi\'raj',
                   'Nisfu Sya\'ban', 'Isra\' Mi\'raj Nabi Muhammad', 'Awal Puasa Ramadhan',
                   'Nuzulul Qur\'an', 'Hari Raya Idul Fitri', 'Wukuf di Arafah', 'Hari Raya Idul Adha'],
    'Definisi': ['Perayaan tahun baru Umat Islam', 'Memperingati peristiwa pengorbanan cucu Nabi Muhammad, Imam Husain',
                 'Memperingati hari diciptakannya Nabi Adam dan hari tobatnya, serta berlabuhnya bahtera Nabi Nuh di bukit Judi',
                 'Memperingati kelahiran Nabi Muhammad SAW', 'Memperingati perjalanan malam Nabi Muhammad dari Makkah ke Yerusalem dan naik ke langit',
                 'Malam pertengahan bulan Sya\'ban', 'Memperingati perjalanan malam Nabi Muhammad dari Makkah ke Yerusalem dan naik ke langit',
                 'Mulai bulan puasa bagi umat Islam', 'Penurunan pertama ayat Al-Qur\'an', 'Perayaan Hari raya Idul Fitri',
                 'Wajib haji berada di Arafah pada tanggal ini', 'Perayaan kurban dan Hari Raya Idul Adha']
}

# Membuat DataFrame dari data kalender hijriyah
df = pd.DataFrame(data)

# Menampilkan judul
st.title('Kalender Hijriyah')

# Membuat input untuk pencarian bulan
search_input = st.text_input('Cari bulan...')

# Melakukan filtering berdasarkan input pencarian
filtered_df = df[df['Bulan'].str.contains(search_input, case=False)]

# Menampilkan hasil filtering
st.table(filtered_df)