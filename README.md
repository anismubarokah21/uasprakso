# uasprakso

# Kalender Hijriyah

Merupakan mesin pencari bulan dari peringatan kalender hijriyah tersebut. Project ini menggunakan bahasa pyhton dan Framework Streamlit.

![](https://gitlab.com/anismubarokah21/praktikumso/-/raw/main/ss/kh.PNG)


# Deskripsi Project

Aplikasi kalender hijriyah ini menyediakan informasi tentang tanggal dan peristiwa penting dalam kalender hijriyah yang memanfaatkan bahasa pemrograman Python dan Framework Streamlit. Aplikasi ini memungkinkan pengguna untuk mencari bulan tertentu dalam kalender hijriyah dan menampilkan peristiwa-peristiwa yang terjadi pada bulan tersebut. Aplikasi ini berguna bagi pengguna yang ingin mengetahui informasi tentang peristiwa-peristiwa penting dalam kalender hijriyah, seperti tanggal-tanggal ibadah, perayaan, atau peringatan dalam agama Islam. Dengan adanya aplikasi ini, pengguna dapat dengan mudah mencari dan melihat informasi tentang kalender hijriyah sesuai kebutuhan mereka.
![](https://gitlab.com/anismubarokah21/praktikumso/-/raw/main/ss/kh1.PNG)
# Penjelasan Bagaimana Sistem Operasi Dalam Proses Containerization

Dalam proses containerization, sistem operasi memainkan peran penting dalam menyediakan lingkungan yang terisolasi untuk menjalankan kontainer. Adapun peran-peran sistem operasi dalam containerization diantaranya :
1. sistem operasi menyediakan isolasi sumber daya antara kontainer, setiap kontainer memiliki tampilan dan akses terpisah ke sumber daya seperti CPU, memori, jaringan, dan penyimpanan.
2. Sistem operasi menyediakan sistem file yang terisolasi untuk setiap kontainer, dalam kontainer setiap kontainer memiliki sistem file root terpisah yang berjalan pada sistem operasi host. 
3. Sistem operasi juga memfasilitasi pengaturan jaringan untuk kontainer, meliputi pengaturan antarmuka jaringan virtual yang terisolasi, pembuatan alamat IP yang unik untuk setiap kontainer, dan pengaturan aturan jaringan yang terpisah. 
4. Sistem operasi memainkan peran penting dalam keamanan kontainer. Melalui teknik namespacing, sistem operasi memastikan bahwa kontainer terisolasi satu sama lain dan dari sistem operasi host. Ini mencegah kontainer dari mengakses sumber daya atau data dari kontainer lain atau host, ini bertujuan untuk melindungi integritas dan kerahasiaan kontainer serta mencegah akses yang tidak sah ke sumber daya dan data.  
5. Sistem operasi bertanggung jawab untuk manajemen proses dalam kontainer. Ini termasuk inisialisasi, menjalankan, dan menghentikan proses dalam kontainer. Sistem operasi menyediakan lingkungan yang terisolasi untuk menjalankan aplikasi dalam kontainer dengan dukungan manajemen proses seperti penjadwalan, pengendalian sumber daya, dan pemantauan. 

Dengan peran-peran ini, sistem operasi memungkinkan kontainer berjalan dalam lingkungan yang terisolasi dan aman, serta mengatur penggunaan sumber daya dengan efisien.

# Penjelasan Bagaimana Containerization dapat Membantu Mempermudah Pengembangan Aplikasi

Containerization dapat membantu mempermudah pengembangan aplikasi dengan beberapa cara, diantaranya :
1. Isolasi Lingkungan, dalam containerization setiap aplikasi dijalankan dalam kontainer terpisah yang memiliki lingkungan yang terisolasi yang artinya setiap kontainer memiliki semua dependensi dan konfigurasi yang diperlukan untuk menjalankan aplikasi secara mandiri. Dengan demikian, pengembang dapat menghindari masalah yang terjadi karena konflik dependensi atau konfigurasi yang bertabrakan antara aplikasi yang berbeda. 
2. Portabilitas, yakni kontainer dapat dijalankan di berbagai platform dan infrastruktur yang mendukung teknologi containerization. Hal ini memungkinkan pengembang untuk mengembangkan aplikasi secara lokal di lingkungan container dan dengan mudah memindahkan aplikasi tersebut ke lingkungan produksi atau infrastruktur lainnya, portabilitas ini mempermudah proses pengembangan, pengujian, dan penyebaran aplikasi.
3. Reproduksi Lingkungan, yakni containerization memungkinkan pengembang untuk mereproduksi lingkungan pengembangan dengan mudah, dengan menggunakan file konfigurasi seperti Dockerfile, pengembang dapat mendefinisikan lingkungan pengembangan yang konsisten untuk seluruh tim. Setiap anggota tim dapat membuat kontainer dengan lingkungan yang sama, termasuk semua dependensi dan konfigurasi yang diperlukan. Hal ini menghilangkan masalah lingkungan yang berbeda di setiap mesin pengembang dan memastikan bahwa aplikasi dapat berjalan dengan konsisten di semua mesin.
4. Skalabilitas, containerization memungkinkan aplikasi untuk dengan mudah diatur secara horizontal atau vertikal, dengan menggunakan teknologi orkestrasi kontainer seperti Kubernetes, pengembang dapat dengan cepat menambahkan atau menghapus instance kontainer untuk meningkatkan kapasitas aplikasi atau menangani lonjakan lalu lintas. Skalabilitas yang mudah ini memudahkan pengembang untuk menguji dan mengelola aplikasi yang skalabel.
5. Pengelolaan Dependensi, dalam containerization dependensi aplikasi dikemas bersama dengan aplikasi dalam kontainer hal ini memungkinkan pengembang untuk dengan mudah mengatur dan mengelola dependensi aplikasi secara terisolasi. Dengan menggunakan manajer dependensi container seperti Docker, pengembang dapat memastikan bahwa dependensi yang diperlukan untuk menjalankan aplikasi tersedia dengan versi yang benar, tanpa perlu khawatir tentang konflik dependensi dengan aplikasi lain.


# Penjelasan Apa Itu DevOps, Bagaimana DevOps Membantu Pengembangan Aplikasi

DevOps adalah pendekatan yang menggabungkan praktik pengembangan perangkat lunak (Development) dan operasi IT (Operations) untuk meningkatkan efisiensi, kolaborasi, dan kualitas dalam siklus pengembangan aplikasi. DevOps mengedepankan kerjasama antara tim pengembang dan tim operasi dengan tujuan menciptakan siklus pengembangan dan penyebaran yang lebih cepat, lebih andal, dan lebih responsif terhadap perubahan.

DevOps membantu pengembangan aplikasi dalam beberapa cara, diantaranya :
1. Kolaborasi Tim yang Ditingkatkan, DevOps mendorong kolaborasi dan komunikasi yang erat antara tim pengembangan dan tim operasi hal ini menghasilkan pemahaman yang lebih baik tentang kebutuhan dan tantangan masing-masing tim. 
2. Automasi dan Otomatisasi, DevOps menerapkan otomatisasi dalam berbagai aspek pengembangan aplikasi, seperti build, pengujian, dan penyebaran. Otomatisasi membantu mengurangi kesalahan manusia, meningkatkan konsistensi, dan mengurangi waktu yang dibutuhkan untuk melakukan tugas-tugas rutin. Dengan adanya otomatisasi, pengembang dapat fokus pada aspek kreatif dan strategis dari pengembangan aplikasi.
3. Pengiriman Cepat dan Responsif, DevOps memungkinkan pengembangan aplikasi dengan siklus pengiriman yang lebih cepat dan responsif. Dengan menggunakan praktik Continuous Integration (CI) dan Continuous Deployment (CD), perubahan kode dapat diintegrasikan dan dideploy secara otomatis dalam waktu singkat hal ini memungkinkan pengembang untuk mendapatkan umpan balik cepat dan secara proaktif merespons perubahan kebutuhan bisnis atau pelanggan.
4. Pemantauan dan Resolusi Masalah yang Cepat, DevOps mendorong penerapan pemantauan dan pemecahan masalah secara terus-menerus dalam siklus pengembangan. Dengan adanya alat pemantauan dan log yang tepat, tim operasi dapat mendeteksi masalah secara real-time dan mengambil tindakan yang cepat untuk memperbaikinya hal ini membantu meningkatkan kualitas dan kehandalan aplikasi serta meminimalkan dampak dari kerusakan atau kegagalan sistem.
5. Scalability dan Resiliensi, DevOps memperhatikan skalabilitas dan ketahanan aplikasi dengan menggunakan teknologi dan praktik yang tepat, seperti kontainerisasi dan orkestrasi kontainer, aplikasi dapat dengan mudah diskalakan dan diatur agar dapat menangani beban yang berat. Selain itu, dengan adanya pemantauan dan pemecahan masalah yang responsif, tim operasi dapat dengan cepat mengatasi masalah dan mengembalikan aplikasi ke keadaan yang normal.

Secara keseluruhan DevOps membantu pengembangan aplikasi dengan meningkatkan kolaborasi tim, menerapkan otomatisasi, memungkinkan pengiriman cepat dan responsif, memantau dan menangani masalah secara proaktif, serta mengoptimalkan skalabilitas dan ketahanan aplikasi. DevOps berfokus pada pengiriman aplikasi yang berkualitas, andal, dan responsif terhadap perubahan, dengan menggabungkan praktik pengembangan dan operasi yang efektif.

# Berikan Contoh Kasus Penerapan DevOps di Perusahaan / Industri Dunia Nyata

Salah satu contoh penerapan DevOps di dunia nyata adalah implementasi DevOps di KAI Access, sebuah perusahaan yang bergerak di bidang teknologi informasi dan pengembangan aplikasi untuk PT Kereta Api Indonesia (Persero) atau biasa dikenal sebagai KAI. Implementasi DevOps di KAI Access membantu meningkatkan efisiensi dan kualitas dalam pengembangan aplikasi mobile untuk layanan penjualan tiket kereta api.

Berikut adalah langkah-langkah implementasi DevOps yang dapat menjadi contoh :
1. Analisis dan Perencanaan, Tim KAI Access menganalisis proses pengembangan aplikasi yang ada, mengidentifikasi area yang dapat ditingkatkan, dan merencanakan implementasi DevOps. Hal ini melibatkan identifikasi alat-alat dan teknologi yang akan digunakan, serta penentuan tim yang akan terlibat dalam implementasi.
2. Kolaborasi Tim, Tim pengembang dan tim operasi KAI Access bekerja sama dalam tim terpadu. Mereka mengadakan pertemuan reguler dan berkomunikasi secara teratur untuk berbagi pengetahuan, pembaruan proyek, dan pemecahan masalah. Kolaborasi tim membantu memastikan pemahaman yang baik antara pengembang dan operasi, serta menghindari silo informasi.
3. Infrastruktur dan Automasi, KAI Access membangun infrastruktur dan mengadopsi alat-alat yang mendukung automasi dan otomatisasi proses pengembangan dan penyebaran hal ini meliputi penggunaan alat-alat seperti Jenkins untuk Continuous Integration, Docker untuk kontainerisasi, Kubernetes untuk orkestrasi kontainer, dan alat pemantauan yang sesuai.
4. Continuous Integration (CI), Tim mengatur pipeline CI menggunakan Jenkins atau alat serupa. Setiap kali pengembang melakukan perubahan kode, CI pipeline memicu otomatisasi pengujian, pengintegrasian, dan pembangunan aplikasi. Ini memastikan bahwa setiap perubahan kode diuji dan diintegrasikan secara otomatis.
5. Continuous Deployment (CD), Setelah perubahan kode berhasil diintegrasikan, CD pipeline diaktifkan. Perubahan kode yang lulus pengujian secara otomatis di-deploy ke lingkungan produksi hal ini memungkinkan rilis aplikasi yang cepat dan konsisten.
6. Pemantauan dan Log, KAI Access memanfaatkan alat pemantauan dan log untuk memantau kinerja aplikasi secara real-time. Data pemantauan dan log digunakan untuk mendeteksi masalah dan mengambil tindakan yang cepat. Tim operasi bertanggung jawab untuk memantau dan menangani masalah dengan responsif.
7. Skalabilitas dan Ketahanan, Infrastruktur KAI Access diatur agar dapat diskalakan secara horizontal atau vertikal sesuai kebutuhan. Kontainerisasi dan orkestrasi kontainer digunakan untuk mengelola dan mengatur skala aplikasi dengan mudah. Dalam situasi kegagalan, tim operasi memiliki rencana pemulihan yang cepat untuk memastikan ketersediaan layanan yang tinggi.
8. Evaluasi dan Peningkatan, KAI Access melakukan evaluasi secara berkala untuk mengukur efektivitas implementasi DevOps. Tim melakukan retrospektif dan melakukan perbaikan berkelanjutan untuk meningkatkan proses, alat, dan kinerja tim.

Implementasi DevOps di KAI Access membantu mempercepat pengembangan dan penyebaran aplikasi, memastikan kualitas yang konsisten, serta meningkatkan responsivitas terhadap perubahan kebutuhan dan masalah yang muncul. Dengan adopsi DevOps, KAI Access dapat memberikan layanan yang lebih baik dan pengalaman pengguna yang lebih baik kepada pelanggan mereka.
