# Nomor 1

Soal lengkap: 

(https://gitlab.com/marchgis/march-ed/2023/courses/if214009-praktikum-sistem-operasi/-/tree/main/business-pitch)

[Gitlab](https://gitlab.com/anismubarokah21/uasprakso/-/tree/main)


# Nomor 2
Mampu mendemonstrasikan pembuatan Docker Image melalui Dockerfile, sebagai bentuk implementasi dari sistem operasi (kumpulkan url Gitlab dari Dockerfile dan file2 pendukungnya)

[Dockerfile](https://gitlab.com/anismubarokah21/uasprakso/-/blob/main/Dockerfile)


# Nomor 3

Mampu mendemonstrasikan pembuatan web page / web service sederhana berdasarkan permasalahan dunia nyata yang dijalankan di dalam Docker Container (kumpulkan url Gitlab dari web page / web servicenya)


[Web Page](http://135.181.26.148:25014/)

![](https://gitlab.com/anismubarokah21/praktikumso/-/raw/main/ss/dockerfie.PNG)
![](https://gitlab.com/anismubarokah21/praktikumso/-/raw/main/ss/dc.PNG)

# Nomor 4

Mampu mendemonstrasikan penggunaan Docker Compose pada port tertentu sebagai bentuk kontainerisasi program dan sistem operasi yang menjadi dasar distributed computing (kumpulkan url Gitlab docker-compose.yml dan folder terkaitnya)


[Docker-compose.yml](https://gitlab.com/anismubarokah21/uasprakso/-/blob/main/docker-compose.yml)


# Nomor 6

Mampu menjelaskan dan mendemonstrasikan web page / web service yang telah dapat diakses public dalam bentuk video Youtube (kumpulkan url video Youtube)

[Link Youtube](https://youtu.be/NHKPc96sKwY)


# Nomor 7

Mampu mempublikasikan Docker Image yang telah dibuat ke Docker Hub, untuk memudahkan penciptaan container sistem operasi di berbagai server pada distributed computing

(kumpulkan url Docker Hub nya)


[Link Docker](https://hub.docker.com/repository/docker/anismubarokah/kalender-hijriyah)
